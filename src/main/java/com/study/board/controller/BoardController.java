package com.study.board.controller;

import com.study.board.model.Board;
import com.study.board.repository.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/board")
public class BoardController {
    @Autowired
    private BoardRepository boardRepository; // @Autowired : 테이블의 데이터 가져오기위헤 boardRepository 인터페이스 필드 주입
    @GetMapping("/list")
//    @ResponseBody
    public String list(Model model){
        List<Board> boards = boardRepository.findAll();
        model.addAttribute("boards", boards);
        // 제대로 올릴거에용
        return "board/list";
    }
    @GetMapping("/manPowerList")
    public String manPowerList(Model model){
        List<Board> boards = boardRepository.findAll();
        model.addAttribute("boards", boards);
        return "board/manPowerList";
    }

    @GetMapping("/form")
    public String form(Model model){
        model.addAttribute("board", new Board());
        return "board/form";
    }
    @PostMapping("/form")
    public String greetingSubmit(@ModelAttribute Board gboard) {
        return "result";
    }
}
